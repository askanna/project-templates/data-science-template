# Data Science template

This is a template for a Data Science project that can be used to start a new project in AskAnna.
Besides the `askanna.yml` this template comes with default directories and files to start a new
data science project.

Using the AskAnna CLI you can run the next command to create a new project in AskAnna with this template:

```bash
askanna create "project name" --template https://gitlab.com/askanna/project-templates/data-science-template.git --push
```


## Project Organization

    ├── askanna.yml        <- Configuration file for AskAnna
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    ├── data
    │   ├── input          <- The original, immutable data (dump) 
    │   ├── interim        <- Intermediate data sets
    │   └── processed      <- The final prepped data sets for modeling
    │
    ├── docs               <- Add Markdown project documentation here (see also section
    |                         'Introduction of Docs')
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- A location for your project related Jupyter notebooks
    │
    ├── result             <- A location for your project related results
    │
    ├── src                <- Source code for use in this project
    │   ├── __init__.py    <- Makes code a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   ├── create_dataset.py
    │   │   └── create_features.py
    │   │
    │   └── models         <- Scripts to train models and then use trained models to serve a result
    │      ├── serve_model.py
    │      └── train_model.py
    │
    ├── .env               <- Environment file for local development
    ├── .gitignore         <- Files or directories that should not be pushed to AskAnna
    └── README.md          <- This document

Using the AskAnna CLI you can run `askanna create` to create a new project in AskAnna.
By default this Blanco template is used. If creating the project is done, you will find
a new local directory with a `askanna.yml` that already contain the url of the new
project.

## Credits

This data science template is inspired by [Cookiecutter Data Science](https://drivendata.github.io/cookiecutter-data-science/).
