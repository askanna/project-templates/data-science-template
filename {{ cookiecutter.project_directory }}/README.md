# {{ cookiecutter.project_name }}

## Project Organization

    ├── askanna.yml        <- Configuration file for AskAnna
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    ├── data
    │   ├── input          <- The original, immutable data (dump) 
    │   ├── interim        <- Intermediate data sets
    │   └── processed      <- The final prepped data sets for modeling
    │
    ├── docs               <- Add Markdown project documentation here (see also section
    |                         'Introduction of Docs')
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- A location for your project related Jupyter notebooks
    │
    ├── result             <- A location for your project related results
    │
    ├── src                <- Source code for use in this project
    │   ├── __init__.py    <- Makes code a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   ├── create_dataset.py
    │   │   └── create_features.py
    │   │
    │   └── models         <- Scripts to train models and then use trained models to serve a result
    │      ├── serve_model.py
    │      └── train_model.py
    │
    ├── .env               <- Environment file for local development
    ├── .gitignore         <- Files or directories that should not be pushed to AskAnna
    └── README.md          <- This document

## Introduction of Docs

This template documentation can be used to add documentation to your project.
Add Markdown files in the docs folder to document your project. The
documentation is based on [Docsify](https://docsify.js.org/). Check out their
site for more information, configuration options and plugins.

### Run the docs locally

If you want to view the project documentation locally, you need a local webserver.
The fastest way is by using Python. Instructions for Python 3.x:

```bash
cd docs && python -m http.server 3000
```

You can also use the Docsify CLI. First install **Node.js**:
[package manager overview](https://nodejs.org/en/download/package-manager/)

Then install Docsify:

```bash
npm i docsify-cli -g
```

And you can run the local server:

```bash
docsify serve docs
```

## Credits

This data science template is inspired by [Cookiecutter Data Science](https://drivendata.github.io/cookiecutter-data-science/).
